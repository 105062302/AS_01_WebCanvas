window.onload = DrawTools;
var _canvas;
var ctx;
var redoUndo = [];
var idx= -1;
var tools = "brush";
var Text = '24px sans-serif';
var font = 'sans-serif';
var size = '14px';
var solidHollow = 'solid';
var last_color;

function push()
{
    idx++;
    if (idx < redoUndo.length) { redoUndo.length = idx; }
    redoUndo.push(document.getElementById('canvas').toDataURL());
}

function fontStyle(str)
{
    font = str;
    Text = size + " " + font;
    ctx.font = Text;
    if(str == 'MingLiU')
        document.getElementById("fontBlock").innerHTML = "細明體";
    else if(str =='PMingLiU')
        document.getElementById("fontBlock").innerHTML = "新細明體";
    else if(str == 'DFKai-sb')
        document.getElementById("fontBlock").innerHTML = "標楷體";
    else if(str == 'Microsoft JhengHei')
        document.getElementById("fontBlock").innerHTML = "微軟正黑體";
    else
        document.getElementById("fontBlock").innerHTML = str;
}

function fontSize(str)
{
    size = str+'px ';
    Text = size + font;
    ctx.font = Text;
    document.getElementById("sizeBlock").innerHTML = str;
}

function DrawTools()
{
    _canvas = document.getElementById('canvas');
    ctx = _canvas.getContext('2d');
    ctx.globalCompositeOperation = 'source-over';
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    
    var x = 0;
    var y = 0;
    var begin_x = 0;
    var begin_y = 0;
    var last_x;
    var last_y;
    var canvasShot;
    var hue = 0;
    var pointMove;

    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };   
    }

    function mouseMove(evt) {
        pointMove = 1;
        if(tools == "brush")
        {   
            ctx.globalCompositeOperation = 'source-over';
            var mousePos = getMousePos(_canvas, evt);
            ctx.lineTo(mousePos.x+2, mousePos.y+14);
            ctx.stroke();
        }
        else if(tools == "eraser")
        {
            ctx.globalCompositeOperation = 'destination-out';
            var mousePos = getMousePos(_canvas, evt);
            ctx.lineTo(mousePos.x+10, mousePos.y+14);
            ctx.stroke(); 
        }
        else if(tools == "rainbow")
        {   
            ctx.globalCompositeOperation = 'source-over';
            ctx.strokeStyle = `hsl(${ hue }, 100%, 50%)`; 
            ctx.beginPath();
            ctx.moveTo(last_x, last_y);  
            if(hue >= 360) hue = 0;
            hue++;
            var mousePos = getMousePos(_canvas, evt);
            ctx.lineTo(mousePos.x + 2, mousePos.y + 14);
            ctx.closePath();
            ctx.stroke();
            last_x = mousePos.x + 2;
            last_y = mousePos.y + 14;
        }
        else if(tools == "circle")
        {
            ctx.globalCompositeOperation = 'source-over';
            var canvasPic = new Image();
            canvasPic.onload = function () { 
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPic, 0, 0);
                var mousePos = getMousePos(_canvas, evt);
                var point_x = (mousePos.x + begin_x)/2, point_y = (mousePos.y + begin_y)/2;
                var radius = Math.sqrt(Math.abs(mousePos.x-begin_x)*Math.abs(mousePos.x-begin_x)+Math.abs(mousePos.y-begin_y)*Math.abs(mousePos.y-begin_y))/2;
                ctx.beginPath();
                ctx.arc(point_x, point_y, radius, 0*Math.PI, 2*Math.PI);
                if(solidHollow == "solid")
                {
                    ctx.fillStyle = ctx.strokeStyle;
                    ctx.fill();
                }
                ctx.closePath();
                ctx.stroke();
            }
            canvasPic.src = canvasShot;
        }
        else if(tools == "rectangle")
        {
            ctx.globalCompositeOperation = 'source-over';
            var canvasPic = new Image();
            canvasPic.onload = function () { 
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPic, 0, 0); 
                var mousePos = getMousePos(_canvas, evt);
                ctx.beginPath();
                if(begin_x > mousePos.x && begin_y >= mousePos.y)
                    ctx.rect(mousePos.x, mousePos.y, begin_x - mousePos.x, begin_y - mousePos.y);
                else if(begin_x > mousePos.x && begin_y < mousePos.y)
                    ctx.rect(mousePos.x, begin_y, begin_x - mousePos.x, mousePos.y - begin_y);
                else if(begin_x < mousePos.x && begin_y >= mousePos.y)
                    ctx.rect(begin_x, mousePos.y, mousePos.x - begin_x, begin_y - mousePos.y);
                else
                    ctx.rect(begin_x, begin_y, mousePos.x - begin_x, mousePos.y - begin_y);
                if(solidHollow == "solid")
                {
                    ctx.fillStyle = ctx.strokeStyle;
                    ctx.fill();
                }
                ctx.stroke();
            }
            canvasPic.src = canvasShot;
        }
        else if(tools == "triangle")
        {
            ctx.globalCompositeOperation = 'source-over';
            var canvasPic = new Image();
            canvasPic.onload = function () { 
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPic, 0, 0); 
                ctx.beginPath();
                var mousePos = getMousePos(_canvas, evt);
                if(begin_y >= mousePos.y)
                {
                    ctx.moveTo(begin_x, begin_y);
                    ctx.lineTo(mousePos.x, begin_y);
                    ctx.lineTo((begin_x+mousePos.x)/2,mousePos.y);
                    ctx.closePath();
                }
                else
                {
                    ctx.moveTo(mousePos.x, mousePos.y);
                    ctx.lineTo(begin_x, mousePos.y);
                    ctx.lineTo((begin_x+mousePos.x)/2,begin_y);
                    ctx.closePath();
                }
                if(solidHollow == "solid")
                {
                    ctx.fillStyle = ctx.strokeStyle;
                    ctx.fill();
                }
                ctx.stroke();
            }
            canvasPic.src = canvasShot;
        }
        else if(tools == "line")
        {
            ctx.globalCompositeOperation = 'source-over';
            var canvasPic = new Image();
            canvasPic.onload = function () { 
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPic, 0, 0);
                ctx.beginPath();
                ctx.moveTo(begin_x, begin_y);
                var mousePos = getMousePos(_canvas, evt);
                ctx.lineTo(mousePos.x, mousePos.y);
                ctx.stroke();
            }
            canvasPic.src = canvasShot;
        }
        else
        {
            pointMove = 0;
        }
    };
    canvas.addEventListener('mousedown', function(evt) {
    pointMove = 0;
    var mousePos = getMousePos(_canvas, evt);
    begin_x = mousePos.x;
    begin_y = mousePos.y;
    evt.preventDefault();
    if(tools == "brush" || tools == "eraser")
        ctx.beginPath();
    else if(tools == "rainbow")
    {
        ctx.beginPath();
        last_x = mousePos.x + 2;
        last_y = mousePos.y + 14;
    }
    if(tools == "circle" || tools == "rectangle" || tools == "triangle" || tools == "line")
    {
        canvasShot = document.getElementById('canvas').toDataURL();
    }
    if(tools == "brush" || tools == "rainbow")
        ctx.moveTo(mousePos.x+2, mousePos.y+14);
    else if(tools == "eraser")
        ctx.moveTo(mousePos.x+10, mousePos.y+14);
    else
        ctx.moveTo(mousePos.x, mousePos.y);
    if(tools != "text")
        canvas.onclick = null;
    canvas.addEventListener('mousemove', mouseMove, false);
    });
    canvas.addEventListener('mouseup', function(evt) {
        canvas.removeEventListener('mousemove', mouseMove, false);
        if((tools == "brush" || tools == "eraser" || tools == "circle" || tools == "triangle" || tools == "rectangle" || tools == "line" || tools == "rainbow") && pointMove == 1)
            push();
        ctx.strokeStyle = last_color;
    }, false);

    document.getElementById("brush-size").addEventListener("change",function(){
        ctx.lineWidth = document.getElementById("brush-size").value;
    } , false);

    ColorPicker(document.getElementById('color-picker'),function(hex, hsv, rgb) {
        ctx.strokeStyle = hex;
        last_color = hex;
    });
}

function selectText(evt)
{
    ctx.globalCompositeOperation = 'source-over';
    var empty = true;

    _canvas.onclick = function(e) {
        if (!empty) return;
        inputText(e.clientX, e.clientY);
    }
    function inputText(x, y) {
        var input = document.createElement('input');
        var rect = _canvas.getBoundingClientRect();
        input.type = 'text';
        input.style.position = 'fixed';
        input.style.left = x;
        input.style.top = y - 10;
        input.onkeydown = handleEnter;
        document.body.appendChild(input);
        input.focus();
        empty = false;
    }

    function handleEnter(e) {
        var keyCode = e.keyCode;
        if (keyCode === 13) {
            drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
            document.body.removeChild(this);
            empty = true;
        }
    }

    function drawText(txt, x, y) {
        var rect = _canvas.getBoundingClientRect();
        ctx.textBaseline = 'top';
        ctx.textAlign = 'left';
        ctx.font = Text;
        ctx.fillStyle = last_color;
        ctx.fillText(txt, x - rect.left, y - rect.top);
        push();
    }
}

function selectUpload()
{
    var picture = document.getElementById('picture');
    picture.addEventListener('change', uploadImage, false);
    function uploadImage(e){
        var reader = new FileReader();
        reader.onload = function(event){
            var img = new Image();
            img.onload = function(){
                _canvas.width = img.width;
                _canvas.height = img.height;
                ctx.drawImage(img,0,0);
            }
            img.src = event.target.result;
        }
        reader.readAsDataURL(e.target.files[0]);     
    }
}

function selectDownload()
{
    var data = canvas.toDataURL("image/png");
    var prev = window.location.href;
    data = data.replace("image/png", "image/octet-stream");
    var link = document.createElement('a');
    link.href = data;
    link.download = "canvas.png";
    var event = document.createEvent('MouseEvents');
    event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    link.dispatchEvent(event);
}

function selectUndo()
{   
    ctx.globalCompositeOperation = 'source-over'; 
    if (idx > 0) {
        idx--;
        var canvasPic = new Image();
        canvasPic.onload = function () { 
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvasPic, 0, 0); 
        }
        canvasPic.src = redoUndo[idx];
    }
}

function selectRedo()
{   
    ctx.globalCompositeOperation = 'source-over'; 
    if (idx < redoUndo.length-1) {
        idx++;
        var canvasPic = new Image();
        canvasPic.onload = function () {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvasPic, 0, 0); 
            }
        canvasPic.src = redoUndo[idx];
    }
}

function selectReset()
{
    ctx.globalCompositeOperation = 'source-over'; 
    if (window.confirm('Are you sure you want to reset ?') == true)
    {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        idx = -1;
        redoUndo.length = 0;
    }
}

function solidSelect()
{
    if(solidHollow == "hollow")
    {
        solidHollow = 'solid';
        $("input#solid").removeClass('hollow');
    }
    else
    {
        solidHollow = 'hollow';
        $("input#solid").addClass('hollow');
    }
}

function toolsSelect(str)
{
    if(str != "undo" && str != "redo" && str != "reset" && str != "download" && str != "upload")
        tools = str;
    if(tools == "brush") $("input#brush").addClass('click');
    else $("input#brush").removeClass('click');
    if(tools == "eraser") $("input#eraser").addClass('click');
    else $("input#eraser").removeClass('click');
    if(tools == "text") $("input#text").addClass('click');
    else $("input#text").removeClass('click');
    if(tools == "circle") $("input#circle").addClass('click');
    else $("input#circle").removeClass('click');
    if(tools == "triangle") $("input#triangle").addClass('click');
    else $("input#triangle").removeClass('click');
    if(tools == "rectangle") $("input#rectangle").addClass('click');
    else $("input#rectangle").removeClass('click');
    if(tools == "line") $("input#line").addClass('click');
    else $("input#line").removeClass('click');
    if(tools == "rainbow") $("input#rainbow").addClass('click');
    else $("input#rainbow").removeClass('click');
    if(tools == "brush")
        document.getElementById('canvas').style.cursor = "url('pic/brush_cur.png'),auto";
    else if(tools == "eraser")
        document.getElementById('canvas').style.cursor = "url('pic/eraser_cur.png'),auto";
    else if(tools == "circle")
        document.getElementById('canvas').style.cursor = "url('pic/circle_cur.png'),auto";
    else if(tools == "triangle")
        document.getElementById('canvas').style.cursor = "url('pic/triangle_cur.png'),auto";
    else if(tools == "rectangle")
        document.getElementById('canvas').style.cursor = "url('pic/rectangle_cur.png'),auto";
    else if(tools == "text")
        document.getElementById('canvas').style.cursor = "text";
    else if(tools == "rainbow")
        document.getElementById('canvas').style.cursor = "url('pic/rainbow_cur.png'),auto";
    else 
        document.getElementById('canvas').style.cursor = "default";
    if(str == "text")
        selectText();
    if(str == "upload")
        selectUpload();
    if(str == "download")
        selectDownload();
    if(str == "undo")
        selectUndo();
    if(str == "redo")
        selectRedo();
    if(str == "reset")
        selectReset();
}
