# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas

### 1. 簡介
這次的作業要使用 html5 中的 canvas 實作出網頁版的小畫家，藉由搭配 css 與 javascript 等語言建構出屬於自己的小畫家。

![](https://i.imgur.com/VsSnueq.png)

### 2. 功能介紹

1. ![](https://i.imgur.com/o57d77O.png) 畫筆(brush) 

小畫家的繪圖功能工具，選取它時可以在畫布上任意的依照自己的意思構圖。

2. ![](https://i.imgur.com/QwRYDti.png) 橡皮擦(eraser)

小畫家的繪圖功能工具，選取它時可以在畫布上將不滿意的地方擦除。

3. ![](https://i.imgur.com/7ovWUPn.png) 文字(text)

小畫家的繪圖功能工具，選取它時可以在畫布上任意的位置輸入文字。

4. ![](https://i.imgur.com/MhLGCLX.png) 圓形(circle)

小畫家的繪圖功能工具，選取它時可以在畫布上任意的位置畫出實心或是空心的圓形。

5. ![](https://i.imgur.com/BeTtAUr.png) 三角形(triangle)

小畫家的繪圖功能工具，選取它時可以在畫布上任意的位置畫出實心或是空心的三角形。

6. ![](https://i.imgur.com/RLFw2AU.png) 方形(rectangle)

小畫家的繪圖功能工具，選取它時可以在畫布上任意的位置畫出實心或是空心的方形。

7. ![](https://i.imgur.com/GDgC3JW.png) 直線(line)

小畫家的繪圖功能工具，選取它時可以在畫布上任意的位置畫出直線。

8. ![](https://i.imgur.com/8pWtYGr.png) 下載(download)

小畫家的畫面控制工具，選取它時可以將當前的畫布轉成 png 格式並儲存在本地端。

9. ![](https://i.imgur.com/0RC2GG7.png) 上傳(upload)

小畫家的畫面控制工具，選取它時可以從本地端上傳一張圖片並將其繪製在畫面上。

10. ![](https://i.imgur.com/UKkoTFK.png) 復原(undo)

小畫家的畫面控制工具，選取它時可以回復到上一步動作。

11. ![](https://i.imgur.com/BTvIvXT.png) 重做(redo)

小畫家的畫面控制工具，選取它時可以回復到當前狀態的下一步動作(如果當前狀態非最後一步)

12. ![](https://i.imgur.com/ri30nF4.png) 重置（reset)

小畫家的畫面控制工具，選取它時可以重置整個畫布。

13. ![](https://i.imgur.com/DMVvh0A.png) ![](https://i.imgur.com/LsLzEoC.png) 實心/空心(solid/hollow)

小畫家的繪圖功能工具，選取他時能控制圓形、三角形、方形在繪畫時實心與空心的切換。

14. ![](https://i.imgur.com/sjGy140.png) 彩虹線(rainbow)

小畫家的繪圖功能工具，選取他時能在畫布上隨意的畫上彩色的線條。

15. ![](https://i.imgur.com/OyGazuY.png) 顏色選取器(color-picker)

小畫家顏色控制工具，透過右側的拉條與左側的顏色選取可以指定畫筆與圖形的顏色。

16.![](https://i.imgur.com/JAjdeat.png) 畫筆尺寸(brush size)

小畫家畫筆控制工具，透過上面的拉桿可以控制畫筆、橡皮擦與圖形的線條粗度。

17. ![](https://i.imgur.com/QXO7HxC.png) 文字字型＆尺寸(font & size)

小畫家字體控制工具，左側選單可以選擇字體，右側選單可以選擇字體尺寸。

### 3. 美工部分介紹

1. 選單控制

![](https://i.imgur.com/1tGLQO2.gif)

選單控制運用 jQuery 中的 addClass 與 removeClass 這兩個函式實作出選取功能時背景變白的特效，並搭配著 hover 讓被游標指到的物件顏色變為白色。

2. 游標控制

![](https://i.imgur.com/nQSpCT9.gif)

游標經由目前的工具做相對應的切換，包含畫筆、橡皮擦、文字、三種圖形以及彩虹線。

### 4. 實作方式

  在 javascript 中運用 getElementById 取得 canvas tag 並搭配著 addEventListener 來監聽 mousedown、mousemove 與 mouseup 三個動作。
當監聽到 mousedown 時，紀錄滑鼠在畫布上的位置，並在此時新增 mousemove 的監聽事件，滑鼠移動時，便會呼叫我寫的 mousemove 函式。而在函式中，我依據當前所選取的功能，分成 brush、eraser、circle、triangle、rectangle、line 與 rainbow 分別設計滑鼠移動時相對應的動作，搭配 beginPath()、moveTo()、lineTo()、closePath()、以及繪製圓形與方形的 arc() 和 rect()，並且透過改變 hsv 的值實作出彩虹線的效果。

線條的色彩控制部分，我藉由偵測 color-picker 滑鼠按下時所取到的顏色，指定給 canvas 的 strokeStyle。

我實作了一個 toolsSelect 的函式，當工具列的任何一個工具被按下都會呼叫一次，並從此函式決定當下被按下的是哪一個工具和這個工具該做什麼事。而在此函式中，我搭配著 jQuery 中的 removeClass 和 addClass ，實作出當工具被選取時，其背景的顏色變化，藉此讓使用者知道當前所選取的是哪一個工具。


### 5. 全體效果一覽

![](https://i.imgur.com/ald5BAx.gif)


























